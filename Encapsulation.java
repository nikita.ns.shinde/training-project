class Student{
	private int sid;
	private String name;
	private String branch;
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
}
public class Encapsulation {

	public static void main(String[] args) {
		Student st=new Student();
		st.setSid(101);
		st.setName("Raj");
		st.setBranch("IT");
		System.out.println("Id is "+st.getSid());
		System.out.println("Name is "+st.getName());
		System.out.println("Branch is "+st.getBranch());
	}

}

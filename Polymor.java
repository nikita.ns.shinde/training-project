class Number{
	void num(int a){
		System.out.println("Calculate");
	}
}
class Check extends Number{
	void num(int a){
		if(a%2==0){
			System.out.println("Even Number");
		}
		else{
			System.out.println("Odd Number");
		}
	}
}
class Calculation extends Number{
	void num(int a){
		System.out.println("Square is "+(a*a));
		System.out.println("Cube  is "+(a*a*a));
	}
}
public class Polymor {

	public static void main(String[] args) {
		Number n;
		n=new Check();
		n.num(20);
		n=new Calculation();
		n.num(5);
	}

}

abstract class Solution
{
	abstract void check(int a);
	void sol(int x,int y)
	{
		System.out.println("Sum is : "+(x+y));
		System.out.println("Difference is : "+(x-y));
		System.out.println("Product is : "+(x*y));
		System.out.println("Division is : "+(x/y));
	}
}
public class Cal extends Solution {
	void check(int a)
	{
		if(a%2==0)
		{
			System.out.println(a+" is Even Number");
		}
		else
		{
			System.out.println(a+" is Odd Number");
		}
	}
	void tab(int n)
	{
		System.out.println("Table of "+n+" is :");
		for(int i=1;i<=10;i++)
		{
			System.out.println(n+" * "+i+" = "+(n*i));
		}
	}
	public static void main(String[] args) {
		Cal c1=new Cal();
		c1.check(20);
		c1.sol(40,20);
		c1.tab(5);
	}
}

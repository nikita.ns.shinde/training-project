import java.io.Serializable; 
import java.io.*;  
class Stdn implements Serializable{  
 int id;  
 String name;  
 public Stdn(int id, String name) {  
  this.id = id;  
  this.name = name;  
 }  
}  
public class Serialize {

	public static void main(String[] args) {
		try{    
			  Stdn s1 =new Stdn(1,"ravi");   
			  FileOutputStream fout=new FileOutputStream("f.txt");  
			  ObjectOutputStream out=new ObjectOutputStream(fout);  
			  out.writeObject(s1);  
			  out.flush();  
			  out.close();  
			  System.out.println("success");  
			  }catch(Exception e){System.out.println(e);}  
	}

}

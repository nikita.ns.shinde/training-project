
public class Overloading {
	void num(int a,int b){
		System.out.println("Sum is "+(a+b));
		System.out.println("Subtraction is "+(a-b));
		System.out.println("Product is "+(a*b));
		System.out.println("Division is "+(a/b));
	}
	void num(int a){
		if(a%2==0){
			System.out.println("Even Number");
		}
		else{
			System.out.println("Odd Number");
		}
	}
	void num(int a,int b ,int c){
		System.out.println("Average of three numbers are "+((a+b+c)/3));
	}
	public static void main(String[] args) {
		Overloading od=new Overloading();
		od.num(20);
		od.num(50,25);
		od.num(20, 30, 40);
	}

}

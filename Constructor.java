
public class Constructor {
	Constructor(){
		System.out.println("Default is running");
	}
	Constructor(int a){
		this();
		if(a%2==0){
			System.out.println("Even Number");
		}
		else{
			System.out.println("Odd Number");
		}
	}
	Constructor(int a ,int b){
		this(20);
		System.out.println("Sum is "+(a+b));
	}
	public static void main(String[] args) {
		Constructor c1=new Constructor(20,10);
	}
}

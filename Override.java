class Num{
	void num(int a ,int b){
		System.out.println("Calculate");
	}
	void check(int a){
		System.out.println("Check");
	}
}
class Addition extends Num{
	void num(int a,int b){
		System.out.println("Sum is "+(a+b));
		System.out.println("Subtraction is "+(a-b));
		System.out.println("Product is "+(a*b));
		System.out.println("Division is "+(a/b));
	}
}
class EvenOdd extends Num{
	void check(int a){
		if(a%2==0){
			System.out.println("Even Number");
		}
		else{
			System.out.println("Odd Number");
		}
	}
}
public class Override extends Addition {

	public static void main(String[] args) {
		Addition ad=new Addition();
		ad.num(100,20);
		EvenOdd ed=new EvenOdd();
		ed.check(30);
	}

}

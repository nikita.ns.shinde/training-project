import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Emp1 {
	static Employee emp1, emp2, emp3;

	public static void main(String[] args) {
		List<Employee> emp = new ArrayList<Employee>();
		emp.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
		emp.add(new Employee(122, "Kaushal Jani", 25, "Male", "Sales And Marketing", 2015, 13500.0));
		emp.add(new Employee(133, "Harshil Nagar", 29, "Male", "Infrastructure", 2012, 18000.0));
		emp.add(new Employee(144, "Raj Darbar", 28, "Male", "Product Development", 2014, 32500.0));
		emp.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
		emp.add(new Employee(166, "Sid Barot", 43, "Male", "Security And Transport", 2016, 10500.0));
		emp.add(new Employee(177, "Pruthvi Soni", 35, "Male", "Account And Finance", 2010, 27000.0));
		emp.add(new Employee(188, "Parth Dabgar", 31, "Male", "Product Development", 2015, 34500.0));
		emp.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
		emp.add(new Employee(200, "Ashish Patel", 38, "Male", "Security And Transport", 2015, 11000.5));
		emp.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
		emp.add(new Employee(222, "Aaalap Patel", 25, "Male", "Product Development", 2016, 28200.0));
		emp.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
		emp.add(new Employee(244, "Divyesh Solanki", 24, "Male", "Sales And Marketing", 2017, 10700.5));
		emp.add(new Employee(255, "Jay Solanki", 23, "Male", "Infrastructure", 2018, 12700.0));
		emp.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
		emp.add(new Employee(277, "Kishan Panchal", 31, "Male", "Product Development", 2012, 35700.0));

		// How many male and female employees are there in the organization?
		// Print the name of all departments in the organization?
		// What is the average age of male and female employees?
		// Get the details of highest paid employee in the organization?
		// Get the names of all employees who have joined after 2015?
		// Get the details of youngest male employee in the product development
		// department?
		// Who has the most working experience in the organization?
		int mcount = 0;
		int fcount = 0;
		int fSalcount = 0;
		int mSalcount = 0;
		double fsum = 0;
		double msum = 0;
		double fsal = 0;
		double msal = 0;
		double maxSal = 0;
		int maxAge = 100;
		int currentYear = 2021;
		Map<String, Integer> map = new HashMap<String, Integer>();
		Set<String> empDept = new HashSet<String>();
		List<String> empDate = new ArrayList<String>();
		List<String> empData = new ArrayList<String>();
		List<String> empData1 = new ArrayList<String>();
		for (Employee e : emp) {
			if (e.getGender().equals("Female")) {
				fcount++;
				fsum = fsum + e.getAge();
			}
			if (e.getGender().equals("Male")) {
				mcount++;
				msum = msum + e.getAge();
			}
			empDept.add(e.getDepartment());
			if (maxSal < e.getSalary()) {
				maxSal = e.getSalary();
				emp1 = e;
			}
			if (e.getYearOfJoining() > 2015) {
				empDate.add(e.getName());
			}
			if (maxAge > e.getAge() && e.getGender().equals("Male")
					&& e.getDepartment().equals("Product Development")) {
				maxAge = e.getAge();
				emp2 = e;
			}
			if (currentYear > e.getYearOfJoining()) {
				currentYear = e.getYearOfJoining();
				emp3 = e;
			}
			if (e.getGender().equals("Female")) {
				fSalcount++;
				fsal = fsal + e.getSalary();
			}
			if (e.getGender().equals("Male")) {
				mSalcount++;
				msal = msal + e.getSalary();
			}
			if(e.getAge() <= 25){
				empData.add(e.getName());
			}
			else{
				empData1.add(e.getName());
			}
		}
		System.out.println("No of male and females are :");
		map.put("Female", fcount);
		map.put("Male", mcount);
		System.out.println(map);
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("No of Departments are :");
		for (String e : empDept) {
			System.out.println(e);
		}
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Average age of Female is :" + (fsum / fcount));
		System.out.println("Average age of Male is :" + (msum / mcount));
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Details of Employee whose Highest Salary is : " + emp1);
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Details of Youngest Male Employee who belongs to Product Development  : " + emp2);
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Details of  Employee who Have more Working experience  : " + emp3);
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Employee Joins After 2015 :");
		for (String e : empDate) {
			System.out.println(e);
		}
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Average Salary of Female is :" + (fsal / fSalcount));
		System.out.println("Average Salary of Male is :" + (msal / mSalcount));
		// Count the number of employees in each department?
		Map<String, Long> employeeCountByDepartment = emp.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("No of Employee Working in each Departments are :");
		for (Map.Entry<String, Long> entry : employeeCountByDepartment.entrySet()) {
			System.out.println("" + entry.getKey() + " : " + entry.getValue());
		}

		// What is the average salary of each department?
		System.out.println("********************************************************");
		System.out.println();
		Map<String, Double> employeeCountByDepartment1 = emp.stream().collect(
				Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));
		System.out.println("Average Salary of each Departments are :");
		for (Map.Entry<String, Double> entry : employeeCountByDepartment1.entrySet()) {
			System.out.println("" + entry.getKey() + " : " + entry.getValue());
		}
		// How many male and female employees are there in the sales and
		// marketing team?
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("Number of male and female employees are there in the sales and marketing team :");
		Map<String, Long> countMaleFemaleEmployeesInSalesMarketing = emp.stream()
				.filter(e -> e.getDepartment() == "Sales And Marketing")
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
		System.out.println(countMaleFemaleEmployeesInSalesMarketing);

		// List down the names of all employees in each department?
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("List of the names of all employees in each department");
		System.out.println("********************************************************");
		System.out.println();
		Map<String, List<Employee>> employeeListByDepartment = emp.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment));

		for (Map.Entry<String, List<Employee>> entry : employeeListByDepartment.entrySet()) {
			System.out.println("Employee in: " + entry.getKey());
			for (Employee employee : entry.getValue()) {
				System.out.println(employee.getName());
			}
			System.out.println();
		}
		System.out.println("********************************************************");
		System.out.println();
		// What is the average salary and total salary of the whole organization?
		System.out.println("Average salary of the whole organization : "+((fsal+msal)/(fcount+mcount)));
		System.out.println("Total salary of the whole organization : "+(fsal+msal));
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("List of Employee Less than or equal to 25 years : ");
		for (String e : empData) {
			System.out.println(e);
		}
		System.out.println("********************************************************");
		System.out.println();
		System.out.println("List of Employee Greater than 25 years : ");
		for (String e : empData1) {
			System.out.println(e);
		}
	}

}

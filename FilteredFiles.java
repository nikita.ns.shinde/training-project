import java.io.File;

public class FilteredFiles {
  public static void main(String[] args) {
    File file = new File("C:/java630");
    File[] fileList = file.listFiles((d,f)-> f.toLowerCase().endsWith(".txt"));
    for(File f : fileList) {
      System.out.println(f.getAbsolutePath());
    }
  }
}
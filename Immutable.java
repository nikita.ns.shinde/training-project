final class Std{
	private int id;
	private String name;
	private String branch;
	Std(int id,String name,String branch){
		this.id=id;
		this.name=name;
		this.branch=branch;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getBranch() {
		return branch;
	}
}
class Immutable {

	public static void main(String[] args) {
		Std s1=new Std(1,"Raj","CS");
		Std s2=new Std(2,"Raju","IT");
		System.out.println("Id: " + s1.getId());
		System.out.println("Name: " + s1.getName());
		System.out.println("Branch: " + s1.getBranch());
		System.out.println("Id: " + s2.getId());
		System.out.println("Name: " + s2.getName());
		System.out.println("Branch: " + s2.getBranch());
	}

}

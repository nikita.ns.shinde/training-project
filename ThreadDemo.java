// Write Program to Explain the concept of Thread Starvation ?

class MyRunnable implements Runnable{
  ThreadDemo td;
  MyRunnable(ThreadDemo td){
    this.td = td;
  }
  public void run() {
    td.testMethod();
    td.testMethod();
    td.testMethod();
  }	
}

class AnotherRunnable implements Runnable{
  ThreadDemo td;
  AnotherRunnable(ThreadDemo td){
    this.td = td;
  }
  public void run() {
    td.testMethod();
  }	
}

public class ThreadDemo {
  public synchronized void testMethod(){
    try {
      System.out.println("Doing some heavy processing for thread " + 
        Thread.currentThread().getName());
      Thread.sleep(300);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
	
  public static void main(String[] args) {		
    ThreadDemo td = new ThreadDemo();
    Thread t1 = new Thread(new MyRunnable(td), "t1");
    Thread t2 = new Thread(new AnotherRunnable(td), "t2");

    t1.setPriority(Thread.MAX_PRIORITY);
    t2.setPriority(Thread.MIN_PRIORITY);

    t1.start();
    t2.start();
  }
}
class Value{
	public static void val(int a){
		if(a>0){
			System.out.println("Positive Number");
		}
		else{
			System.out.println("Negative Number");
		}
	}
}
class Value2 extends Value{
	public static void val(int a){
		System.out.println("Square is "+(a*a));
		System.out.println("Cube is "+(a*a*a));

	}
}
public class Static {
	public static void num(int a){
		if(a%2==0){
			System.out.println("Even Number");
		}
		else{
			System.out.println("Odd Number");
		}
	}
	public static void num(int a,int b){
		System.out.println("Sum is "+(a+b));
		System.out.println("Subtraction is "+(a-b));
		System.out.println("Product is "+(a*b));
		System.out.println("Division is "+(a/b));
	}
	public static void main(String[] args) {
		Static.num(15);
		Static.num(50);
		Static.num(40,20);
		Value v=new Value2();
		v.val(-20);
		v.val(20);
		Value2 v1=(Value2) v;
		v1.val(7);
	}

}

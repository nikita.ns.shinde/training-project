class A{
	void display(){
		System.out.println("A is running");
	}
}
class B extends A{
	void display(){
		System.out.println("B is running");
	}
}
public class Hierar extends A {
	void display(){
		System.out.println("Hierar is running");
	}
	public static void main(String[] args) {
		Hierar h=new Hierar();
		h.display();
		B b=new B();
		b.display();
	}
}

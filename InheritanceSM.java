class Calc
{
	void add(int a,int b)
	{
		System.out.println("Sum is : "+(a+b));
	}
	void sub(int a,int b)
	{
		System.out.println("Difference is : "+(a-b));
	}
	void mul(int a,int b)
	{
		System.out.println("Multiplication is : "+(a*b));
	}
	void div(int a,int b)
	{
		System.out.println("Division is : "+(a/b));
	}
}
class Calc2 extends Calc
{
	void square(int a)
	{
		System.out.println("Square is : "+(a*a));	
	}
	void cube(int a)
	{
		System.out.println("Cube is : "+(a*a*a));	
	}
}
public class InheritanceSM extends Calc2 {
	void show(){
		System.out.println("******* CALCULATOR ******");
	}
	public static void main(String[] args) {
		InheritanceSM a=new InheritanceSM();
		a.show();
		a.add(20,30);
		a.sub(30,20);
		a.mul(10,20);
		a.div(100,5);
		a.square(4);
		a.cube(6);
	}

}

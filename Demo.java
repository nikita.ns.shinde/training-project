class Dummy
{
	void add(int a,int b)
	{
		System.out.println("Sum is : "+(a+b));
	}
}
interface Base
{
	void check(int a);
	static Dummy d=new Dummy();
}
class Demo implements Base
{
	public void check(int a)
	{
		if(a%2==0)
		{
			System.out.println("Even Number");
		}
		else
		{
			System.out.println("Odd Number");
		}
	}
	public static void main(String gg[])
	{
		Base b1=new Demo();
		Base.d.add(10,20);
		b1.check(20);
	}
}
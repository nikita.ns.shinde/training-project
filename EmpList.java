import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class EmpList {

	public static void main(String[] args) {
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2018, 25000.0));
		employeeList.add(new Employee(122, "Kaushal Jani", 25, "Male", "Sales And Marketing", 2015, 13500.0));
		employeeList.add(new Employee(133, "Harshil Nagar", 29, "Male", "Infrastructure", 2012, 18000.0));
		employeeList.add(new Employee(144, "Raj Darbar", 28, "Male", "Product Development", 2014, 32500.0));
		employeeList.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2017, 22700.0));
		employeeList.add(new Employee(166, "Sid Barot", 43, "Male", "Security And Transport", 2016, 10500.0));
		
		//How many male and female employees are there in the organization?
		Map<String, Long> noOfMaleAndFemaleEmployees = employeeList.stream()
		        .collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
		 
		System.out.println(noOfMaleAndFemaleEmployees);
		
		//Print the name of all departments in the organization?
		employeeList.stream()
	    .map(Employee::getDepartment)
	    .distinct()
	    .forEach(System.out::println);
		
		//Print the name of all departments in the organization?
		Map<String, Double> avgAgeOfMaleAndFemaleEmployees=
		        employeeList.stream().collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingInt(Employee::getAge)));
		 
		System.out.println(avgAgeOfMaleAndFemaleEmployees);
		
		//Get the details of highest paid employee in the organization?
		Optional<Employee> optional = employeeList.stream()
			    .max((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary()));
			System.out.println(optional.get());
			 
			Optional<Employee> highestPaidEmployeeWrapper = employeeList.stream()
			    .collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary)));
			System.out.println(highestPaidEmployeeWrapper.get());
			
			//Get the names of all employees who have joined after 2015?
			employeeList.stream()
		    .filter(e -> e.getYearOfJoining() > 2015)
		    .map(Employee::getName)
		    .forEach(System.out::println);
			//Count the number of employees in each department?
			Map<String, Long> employeeCountByDepartment = employeeList.stream()
				    .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
				 
				Set<Entry<String, Long>> entrySet = employeeCountByDepartment.entrySet();
				 
				for (Entry<String, Long> entry : entrySet) {
				    System.out.println(entry.getKey()+" : "+entry.getValue());
				}
				
				//What is the average salary of each department?
				Map<String, Double> employeeCountByDepartment1 = employeeList.stream()
					    .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));
					 
					Set<Entry<String, Double>> entrySet1 = employeeCountByDepartment1.entrySet();
					 
					for (Entry<String, Double> entry : entrySet1) {
					    System.out.println(entry.getKey()+" : "+entry.getValue());
					}
					//Get the details of youngest male employee in the product development department?
					Optional<Employee> youngestMaleEmployeeInProductDevelopmentWrapper = employeeList.stream()
						    .filter(e -> e.getGender().equals("Male") && e.getDepartment().equals("Product Development"))
						    .min((e1, e2) -> e1.getAge() - e2.getAge());
						System.out.println(youngestMaleEmployeeInProductDevelopmentWrapper.get());
						
						//Who has the most working experience in the organization?
						Optional<Employee> seniorMostEmployeeWrapper = employeeList.stream()
							    .sorted(Comparator.comparingInt(Employee::getYearOfJoining)).findFirst();
							System.out.println(seniorMostEmployeeWrapper.get());
							 
							seniorMostEmployeeWrapper = employeeList.stream()
							    .sorted((e1, e2) -> e1.getYearOfJoining() - e2.getYearOfJoining()).findFirst();
							System.out.println(seniorMostEmployeeWrapper.get());
							 
							seniorMostEmployeeWrapper = employeeList.stream()
							    .min(Comparator.comparingInt(Employee::getYearOfJoining));
							System.out.println(seniorMostEmployeeWrapper.get());
							 
							seniorMostEmployeeWrapper = employeeList.stream()
							    .min((e1, e2) -> e1.getYearOfJoining() - e2.getYearOfJoining());
							System.out.println(seniorMostEmployeeWrapper.get());
							//How many male and female employees are there in the sales and marketing team?
							Map<String, Long> countMaleFemaleEmployeesInSalesMarketing = employeeList.stream()
								    .filter(e -> e.getDepartment() == "Sales And Marketing")
								    .collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
								System.out.println(countMaleFemaleEmployeesInSalesMarketing);
								//What is the average salary of male and female employees?
								Map<String, Double> avgSalaryOfMaleAndFemaleEmployees = employeeList.stream()
									    .collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingDouble(Employee::getSalary)));
									System.out.println(avgSalaryOfMaleAndFemaleEmployees);
									//List down the names of all employees in each department?
									
									//What is the average salary and total salary of the whole organization?
									DoubleSummaryStatistics employeeSalaryStatistics = employeeList.stream()
										    .collect(Collectors.summarizingDouble(Employee::getSalary));
										 
										System.out.println("Average Salary: " + employeeSalaryStatistics.getAverage());
										System.out.println("Total Salary: " + employeeSalaryStatistics.getSum());
										
										//Separate the employees who are younger or equal to 25 years from those employees who are older than 25 years?
										Map<Boolean, List<Employee>> partitionEmployeesByAge = employeeList.stream()
											    .collect(Collectors.partitioningBy(e -> e.getAge() > 25));
											 
											Set<Entry<Boolean, List<Employee>>> entrySet11 = partitionEmployeesByAge.entrySet();
											for (Entry<Boolean, List<Employee>> entry : entrySet11) {
											    if (entry.getKey()) {
											        System.out.println("Employees older than 25 years :");
											    } else {
											        System.out.println("Employees younger than or equal to 25 years :");
											    }
											    List<Employee> list = entry.getValue();
											    for (Employee e : list) 
											    {
											        System.out.println(e.getName());
											    }
											    System.out.println();
											}

				//Who is the oldest employee in the organization? What is his age and which department he belongs to?
											Optional<Employee> oldestEmployeeWrapper = employeeList.stream()
												    .sorted((e1, e2) -> e2.getAge() - e1.getAge()).findAny();
												System.out.println(oldestEmployeeWrapper.get());
												 
												oldestEmployeeWrapper = employeeList.stream()
												    .max((e1, e2) -> e1.getAge() - e2.getAge());
												System.out.println(oldestEmployeeWrapper.get());
	}

}
